variable "project_id" {
    default = "test-chronotruck"
}

variable "region" {
    default = "europe-west1"
}