# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/archive" {
  version = "2.3.0"
  hashes = [
    "h1:pTPG9Kf1Qg2aPsZLXDa6OvLqsEXaMrKnp0Z4Q/TIBPA=",
    "zh:0869128d13abe12b297b0cd13b8767f10d6bf047f5afc4215615aabc39c2eb4f",
    "zh:481ed837d63ba3aa45dd8736da83e911e3509dee0e7961bf5c00ed2644f807b3",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9f08fe2977e2166849be24fb9f394e4d2697414d463f7996fd0d7beb4e19a29c",
    "zh:9fe566deeafd460d27999ca0bbfd85426a5fcfcb40007b23884deb76da127b6f",
    "zh:a1bd9a60925d9769e0da322e4523330ee86af9dc2e770cba1d0247a999ef29cb",
    "zh:bb4094c8149f74308b22a87e1ac19bcccca76e8ef021b571074d9bccf1c0c6f0",
    "zh:c8984c9def239041ce41ec8e19bbd76a49e74ed2024ff736dad60429dee89bcc",
    "zh:ea4bb5ae73db1de3a586e62f39106f5e56770804a55aa5e6b4f642df973e0e75",
    "zh:f44a9d596ecc3a8c5653f56ba0cd202ad93b49f76767f4608daf7260b813289e",
    "zh:f5c5e6cc9f7f070020ab7d95fcc9ed8e20d5cf219978295a71236e22cbb6d508",
    "zh:fd2273f51dcc8f43403bf1e425ba9db08a57c3ddcba5ad7a51742ccde21ca611",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version = "4.63.0"
  hashes = [
    "h1:YEKH5Dyov4Vs9deALoKxsupIn7Y72mPQQqhJVRPC9TM=",
    "zh:27080a67f69e7b812710da6d6aca0099f2319ce42d882773eaa5968eacb06eac",
    "zh:30ba091dde436afdbea2bf1e5ee1401e2d767517671eb378e94f52c57a63ef8b",
    "zh:32e539cbdde2540a12bc4351a8715ebe82b717891e7f1a6b2027c3ea82c234f5",
    "zh:53c1fd99074c718934a23329033adaa70109c9b6ccc8c032f8b7ad08e175593b",
    "zh:653720fe83b5889de4cb572894e2f9704c463fb0293c943ca201caca53081ce7",
    "zh:689bdd6ad3dd35dde20abf38c52fdcd16fe2f8604a526f5f9c2cad0219fe902c",
    "zh:a74c95639449fb19847d6ac028202c3b29700ccae86944159676ce5e68da12f8",
    "zh:ae3c46342ddfe28c59e46d53304eb4a27b9c72a7903553412c8249071aae0dcd",
    "zh:bbafce4c592d28943dd235ecf47d5bded35964ae946234379b97d049cbcbe521",
    "zh:dbcb5f1c602909341dd86a12cebcc00e6e3be4793fccc75695df5333aff9dc00",
    "zh:ecf81a806e62f284dfae5c0ae9834bc24bb2825c910cbe8b2fa63e28c1ccd24b",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}
